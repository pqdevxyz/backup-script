
<#FLAG LIST:
C - Copy Only (Does Not Mirror between Source and Dest)
P - Mirror Source to Dest
M - Move (Moves data from Source to Dest)

L - Local Only (Local Machine NOT over network)
N - Network (Network Copying (eg to NAS))

# - Skip (Skips over that line)

E.G. CL - Copys files Local

NOTE: Try to stick to C/P/M first then L/N! and only use CAPITALS, you need to set both the type of copy you want to make and if it is local or networked
for logs: /NP /NC /NFL /NS /NDL
#>

function Get-ScriptDirectory {
    Split-Path -parent $PSCommandPath
}

function Write-Color([String[]]$Text, [ConsoleColor[]]$Color) {
    for ($i = 0; $i -lt $Text.Length; $i++) {
        Write-Host $Text[$i] -Foreground $Color[$i] -NoNewLine
    }
    Write-Host
}

$scriptFolder = Get-ScriptDirectory

$lastRunPath = Join-Path ${scriptFolder} "\lastRun.txt"
$config = Import-Csv (Join-Path ${scriptFolder} "\config.csv")
$logPath = Join-Path ${scriptFolder} "\backupLog.txt"

Write-Color " Version 1.2.1b" Green

if(!(Test-Path $lastRunPath))
{
   "" | Out-File -Force -FilePath $lastRunPath
}
$item = Get-Item $lastRunPath  -Force

$lastRun = Get-Content $item
if(!((Get-Date).Day -eq $lastRun))
{
    Write-Color " Clearing Log File" Red
    Remove-Item $logPath
    # run clean log code
}

$item.Attributes = 'Normal'
(Get-Date).Day | Out-File -Force -filepath $lastRunPath
$item.Attributes = 'Hidden'

Write-Color " Starting Backup..." Green
foreach ($line in $config)
{
    $source = $($line.Source)
    $dest = $($line.Dest)
    $flag= $($line.Flag)
    $time = Get-Date -format T

    if(($flag -eq "PN") -or ($flag -eq "NP")){
        # robocopy mirror with network
        Write-Color " Started Mirroring From ", $source, " To ", $dest, " Over Network At: ", $time Cyan,Yellow,Cyan,Magenta,Cyan,Cyan
        robocopy $source $dest /FFT /E /PURGE /R:3 /W:60 /Z /MT:4 /UNILOG+:$logPath /NS /TEE
        Write-Host ""
        Write-Color " Finished Mirroring From ", $source, " To ", $dest, " Over Network At: ", $time Cyan,Yellow,Cyan,Magenta,Cyan,Cyan
        Write-Host ""
    }
    elseif(($flag -eq "PL") -or ($flag -eq "LP")){
        # robocopy mirror without network
        Write-Color " Started Mirroring From ", $source, " To ", $dest, " At: ", $time Cyan,Yellow,Cyan,Magenta,Cyan,Cyan
        robocopy $source $dest /E /PURGE /R:3 /W:5  /MT:64 /UNILOG+:$logPath /NS /TEE
        Write-Host ""
        Write-Color " Finished Mirroring From ", $source, " To ", $dest, " At: ", $time Cyan,Yellow,Cyan,Magenta,Cyan,Cyan
        Write-Host ""
    }
     elseif(($flag -eq "CN") -or ($flag -eq "NC")){
        # robocopy copy with network
        Write-Color " Started Copying From ", $source, " To ", $dest, " Over Network At: ", $time Green,Yellow,Green,Magenta,Green,Green
        robocopy $source $dest /FFT /E  /R:3 /W:60 /Z /MT:4 /UNILOG+:$logPath /NS /TEE
        Write-Host ""
        Write-Color " Finished Copying From ", $source, " To ", $dest, " Over Network At: ", $time Green,Yellow,Green,Magenta,Green,Green
        Write-Host ""
    }
     elseif(($flag -eq "CL") -or ($flag -eq "LC")){
        # robocopy copy without network
        Write-Color " Started Copying From ", $source, " To ", $dest, " At: ", $time Green,Yellow,Green,Magenta,Green,Green
        robocopy $source $dest /E  /R:3 /W:5 /MT:64 /UNILOG+:$logPath /NS /TEE
        Write-Host ""
        Write-Color " Finished Copying From ", $source, " To ", $dest, " At: ", $time Green,Yellow,Green,Magenta,Green,Green
        Write-Host ""
    }
     elseif(($flag -eq "MN") -or ($flag -eq "NM")){
        # robocopy move with network
        Write-Color " Started Moving From ", $source, " To ", $dest, " Over Network At: ", $time Red,Yellow,Red,Magenta,Red,Red
        robocopy $source $dest /FFT /MOVE  /R:3 /W:60 /Z /UNILOG+:$logPath /NS /TEE
        Write-Host ""
        Write-Color " Finished Moving From ", $source, " To ", $dest, " Over Network At: ", $time Red,Yellow,Red,Magenta,Red,Red
        Write-Host ""
    }
     elseif(($flag -eq "ML") -or ($flag -eq "LM")){
        # robocopy move without network
        Write-Color " Started Moving From ", $source, " To ", $dest, " At: ", $time Red,Yellow,Red,Magenta,Red,Red
        robocopy $source $dest /E /R:3 /W:5 /MOVE /UNILOG+:$logPath /NS /TEE
        Write-Host ""
        Write-Color " Finished Moving From ", $source, " To ", $dest, " At: ", $time Red,Yellow,Red,Magenta,Red,Red
        Write-Host ""
    }
    elseif($flag -eq "#"){
        Write-Color " Skipped Line" White
        Write-Host ""
    }
    else{
        Write-Color " ERROR MISSING FLAG INFO!" Magenta
        Write-Host ""
    }
 }



Write-Color " Finished Script. Press any key to exit..." Green
[void][System.Console]::ReadKey($true)
