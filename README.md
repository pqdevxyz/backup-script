# Powershell Backup Script V1.2.1
*Current release version:* **V1.2.1**
*Current beta version:* **V1.3.1**

### **Please Note as of V1.2.1 You DO NOT need to run as administrator**

## Installation
Copy all files into a new folder that you would like to run the backup from. They should be:
* config.csv
* backup.bat
* backup.ps1
* *README.md (This file)*

Next open the config.csv and add the folders you want to back up (see below):
```
Source,Dest,Flag
[Source Folder],[Destination Folder],[Flags]
```
An example of this could be:
```
D:\My Documents\2016,F:\Docs Backup\2016,PL
```

In the example above the Source would be *"D:\My Documents\2016"*.

And the Destination would be *"F:\Docs Backup\2016"*.

Next set the *"Flags"* you would like to use, these can be any of the following:

* *CL* - This means **copy locally** *(e.g. copying from one drive to another on the same machine).*
* *CN* - This means **copy over the network** *(e.g. copying from one machine to another machine on the same network).*
* *PL* - This means **mirror locally** *(e.g. mirroring from one drive to another on the same machine).*
* *PN* - This means **mirror over the network** *(e.g. mirroring from one machine to another machine on the same network).*
* *ML* - This means **move locally** *(e.g. moving from one drive to another on the same machine).*
* *MN* - This means **move over the network** *(e.g. moving from one machine to another machine on the same network).*
* *#* - This means skip that line in the config file and the folder **won't** be backup.
> Make sure to set a **Source, Destination** and the **Flags** correctly before running the Script.

## Running the Script
1. Double click on the *"backup.bat"* file in the folder you copied the backup scipt to.

2. The script will now start to run.

### Optional Step (Set up a task to run automatically)
If you would like to have the backup run at a set time during the day or at another type of trigger then all you need to do is set it up in *"Task Scheduler"*.
> Please note that the instruction below are for **Windows 10 only**.

To open *"Task Scheduler"* do the following:

1. Right click on the windows button.

2. Click on *"Computer Management"*.

3. When the window opens click on *"Task Scheduler"* in the left hand side pannel.

4. On the right hand side pannel click *"Create Task"*.

5. The Create Task window should now open (set up the following):

    1. Name.

    2. Set *"Configure for:"* to Windows 10.

    3. Under the *"Triggers"* tab add the triggers you would like to use the start the task.

    4. Under the *"Actions"* tab set the new action to start a program, set the path to that of the *roboBackupNewPS.bat* in the folder you copied the files to e.g. *"D:\My Documents\Backup\roboBackupNewPSb.bat"*.

    5. Click on the *"Ok"* button.

You have now set up the backup to run automatically.

### Using the beta version
In order to use the beta version just use backupBeta.bat instead of backup.bat.

**However please note that the beta version my break and no support is given. Use at you own risk!**